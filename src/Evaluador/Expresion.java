/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;
import ufps.util.colecciones_seed.*;
/**
 *
 * @author estudiante
 */
public class Expresion {
    
  private ListaCD<String> expresiones=new ListaCD();  

    public Expresion() {
    }
    
    public Expresion(String cadena) {
        
       String v[]=cadena.split(",");   //LO CONVIERTO A ARREGLO    ESTO ES UNA LINEA EJM sin (,2,+,2,),*,3
       for(String dato:v)               // INSERTO EL ARREGLO EN LA LISTA
           this.expresiones.insertarAlFinal(dato);          // ahora la linea es una es una lista
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
     String msg="";
     
     for(String dato: this.expresiones)
         msg+=dato+"<->";
    return msg;
    }
    
   public String mostrarResultados(){
          String [] postfijo = this.getPosfijo().split(",");
          String resPost = "";
          
          for (String dato: postfijo) 
            resPost+=dato;
          
          return "Postfijo = "+resPost+"\n"+
                 "Prefijo  = "+this.getPrefijo() +"\n"+
                 "resultado = "+getEvaluarPosfijo();
    }
    
    public String getPrefijo()
    {
        //https://www.youtube.com/watch?v=kzuplp4NVvY
        ListaCD<Character> expInv = invertirExpresion();
        Pila<Character> pila = new Pila();
        String prefijoInv = "";
        String prefijo = "";
        
        for(char dato:expInv)
        {        
            
            if(dato == '/' || dato == '*' || dato == '-' || dato == '+' || dato == ')')
            {
                pila.apilar(dato);
            }
            if(dato != '/' && dato != '*' && dato != '-' && dato != '+' && dato != ')' && dato != '(')
            {
                prefijoInv += dato;
            }
            if(dato == '(')
            {
                char valorTope;
                do
                {
                    valorTope = pila.desapilar();
                    if(valorTope == '/' || valorTope == '*' || valorTope == '-' || valorTope == '+')
                        prefijoInv += valorTope;
                }while(valorTope != ')');  
                if(!pila.esVacia())
                {
                valorTope = pila.desapilar();
                if(valorTope =='*'|| valorTope =='/')
                    {
                        prefijoInv += valorTope;
                    }else{
                        pila.apilar(valorTope);
                    }
                }
                
            }
            
        }        
        while(!pila.esVacia())
        {
            prefijoInv = prefijoInv + pila.desapilar();           
        }
        //inversion de la expresion
        for(int i = prefijoInv.length()-1;i>=0;i--)
        {
            prefijo = prefijo + prefijoInv.charAt(i);
        }
        return prefijo;
    }
    
    private ListaCD<Character> invertirExpresion()
    {
        {
        if(this.expresiones.esVacia())
        {
            return null;
        }
        
        ListaCD<Character> expInv = new ListaCD();
        String expresion = "";
        for(String dato:this.expresiones)
        {
            expresion = expresion + dato;
        }
        for(int i = expresion.length()-1;i>=0;i--)
        {
            expInv.insertarAlFinal(expresion.charAt(i));            
        }
        return expInv;
        }
    }
    
    public boolean esSigno(String c){
    
        if(c.equals("(") || c.equals("+") || c.equals("-") || c.equals("/") || c.equals("*") || c.equals(")") )
            return true;
            return false;
    }
    
    public String getPosfijo()
    {
         Pila<String> p = new Pila();
         String postfijo="";
         String d = "";
         
         for (String dato: this.expresiones) {
            
             if (esSigno(dato)) { /////---------------------------------------------
                 
                 if(dato.equals("("))
                     p.apilar(dato);
                 
                 if(dato.equals("-") || dato.equals("/") || dato.equals("*") || dato.equals("+")){//****************
                     if(!p.esVacia()){
                     d = p.desapilar();
                     if(tienePrecedencia(d, dato)){
                         if(p.esVacia())
                             postfijo+=d+",";
                         else{
                         while(!d.isEmpty()){
                             postfijo+=d+",";
                             d=p.desapilar();
                         } 
                         }
                       } else p.apilar(d);
                     }
                     p.apilar(dato);
                 }//***********************************************************
                 if(dato.equals(")")){
                     d = p.desapilar();
                     while(!d.equals("(")){
                         postfijo+=d+",";
                         d = p.desapilar();
                     }
                 }
             } //---------------------------------------------------
             else{
                 postfijo+=dato+",";}
             }
         
             while(!p.esVacia())
                 postfijo+=p.desapilar()+",";
             
             return postfijo;
        }
        
        
    public boolean tienePrecedencia(String antes, String dato){
        if((dato.equals("-") || dato.equals("+")) && (antes.equals("*") || antes.equals("/")))
            return true;
        return false;
    }
    
    
    public float getEvaluarPosfijo()
    {
        String []postfijo = this.getPosfijo().split(",");
        Pila<Float> p = new Pila();
        float operando1=0;
        float operando2=0;
        float resultado = 0;
        
        for (int i = 0; i<postfijo.length; i++) {
            
            if(!esSigno(postfijo[i])){
            p.apilar(Float.parseFloat(postfijo[i]));
            }
            else{
                operando2 = p.desapilar();
                operando1 = p.desapilar();
                
                switch(postfijo[i]){
                
                    case "-":{
                        resultado = operando1-operando2;
                        break;
                    }
                    case "+" :{
                        resultado = operando1+operando2;
                        break;
                    }
                    case "*" :{
                        resultado = operando1*operando2;
                        break;
                    }
                    case "/" :{
                        resultado = operando1/operando2;
                        break;
                    }
                
                }
               p.apilar(resultado);
            }
            
        }
        return p.desapilar();
    }
}
